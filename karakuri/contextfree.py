from collections import namedtuple
from .regular import wrap_accepted_states, DerivationGraph, epsilon, multi_transition, StateDiagram
from dataclasses import dataclass, astuple
from typing import TypeVar, Generic, Optional, List, Tuple, Collection, Dict, \
        Iterable, Iterator, Callable, Set, FrozenSet, Mapping, Sequence, cast, \
        AbstractSet
import typing
from typing_extensions import Protocol
from collections import Counter
from abc import abstractmethod
from typing import Any, TypeVar
# Source: https://stackoverflow.com/a/37669538/2327050
class Comparable(Protocol):
    @abstractmethod
    def __lt__(self, other: Any) -> bool: ...

TComparable = Comparable #typing.Union[Comparable, int, str]

CT = TypeVar('CT', bound=TComparable)
S = TypeVar('S', bound=TComparable)
B = TypeVar('B', bound=TComparable)
A = TypeVar("A", bound=TComparable)

def opt_lt(o1:Optional[CT], o2:Optional[CT]) -> bool:
    if o1 is None and o2 is None:
        return False
    if o1 is None:
        return True
    if o2 is None:
        return False
    return o1 < o2


REST = 0
TOP = 1

Stack = Tuple[A,...]

def stack_push(stack:Stack[A], arg:Optional[A]) -> Stack[A]:
    if arg is None:
        return stack
    return stack + (arg,)

@dataclass(frozen=True)
class Transition(Generic[S,B]):
    push_arg:Optional[B]
    dst:S

    def __lt__(o1, o2:"Transition[S,B]") -> bool:
        arg_lt = opt_lt(o1.push_arg, o2.push_arg)
        return \
            arg_lt or \
            (o2.push_arg == o2.push_arg and o1.dst < o2.dst)

    def __iter__(self) -> Tuple[Optional[B], S]:
        # Unfortunately, typing info of astuple is not smart as we would like
        # So, we must cast it
        return cast(Tuple[Optional[B], S], astuple(self))


@dataclass(frozen=True, order=True)
class StackState(Generic[S,B]):
    state: S
    stack: Stack[B]

    def pop(self, arg:Optional[B]) -> Optional[Stack[B]]:
        if arg is None:
            return self.stack
        return self.stack[:-1] if len(self.stack) > 0 and self.stack[-1] == arg else None

    def advance(self, arg:Optional[B], t:Transition[S,B]) -> Optional["StackState[S,B]"]:
        stack = self.pop(arg)
        if stack is not None:
            stack = stack_push(stack, t.push_arg)
            return StackState(stack=stack, state=t.dst)
        return None



InTransition = Tuple[S,Optional[A],Optional[B]]
OutTransitions = typing.Union[Transition[S,B],Sequence[Transition[S,B]]]
PTransitionTable = Mapping[
    InTransition[S,A,B],
    OutTransitions[S,B],
]
StateDiagram_Edge = Tuple[Optional[A],Optional[B],Optional[B]]


T1 = TypeVar("T1")
T2 = TypeVar("T2")
def cons(x:T1, it:Iterator[T2]) -> Iterator[typing.Union[T1,T2]]:
    yield x
    yield from it

class EpsilonOverflowError(Exception):
    def __init__(self, state):
        self.state = state


@dataclass
class PDANode(Generic[S,A, B]):
    fuel: int
    index: int
    state: StackState[S,B]


    def is_final(self, pda, word):
        return len(word) == self.index and pda.accepted_states(self.state.state)

    def get_key(self):
        return (self.index, self.state)

    def get_next(self, tsx, accepted, word):
        if self.fuel <= 0:
            return
        if self.index < len(word):
            for x in tsx(self.state, word[self.index]):
                yield PDANode(
                    fuel = self.fuel - 1,
                    index = self.index + 1,
                    state = x,
                )
        for x in tsx(self.state, None):
            yield PDANode(
                fuel = self.fuel - 1,
                index = self.index,
                state = x,
            )

    @classmethod
    def make_start(cls, pda, word, fuel):
        return cls(
            state = pda.start_stack_state,
            fuel = fuel,
            index = 0,
        )

#############################################3

@dataclass(eq=True, frozen=True)
class Node(Generic[S, A]):
    index: int
    state: S

    @classmethod
    def make_start(cls, pda, word):
        return cls(
            state = pda.start_state,
            index = 0,
        )

    def __lt__(self, other):
        return (self.index, self.state) < (other.index, other.state)

@dataclass(eq=True, frozen=True)
class Action(Generic[A,B]):
    read_char: Optional[A]
    pop_char: Optional[B]
    push_char: Optional[B]

    def can_advance(self):
        return self.pop_char is None

    def skip(self):
        return self.pop_char is None and self.push_char is None

    def only_push(self):
        return self.push_char is not None and self.pop_char is None

    def only_pop(self):
        return self.push_char is None and self.pop_char is not None

    def connects_to(self, other):
        return (
            self.push_char is not None and
            self.push_char == other.pop_char
        )

    def get_increment(self):
        return 0 if self.read_char is None else 1


    def add(self, other:"Action[A,B]"):
        if self.skip():
            return other
        if other.skip():
            return self
        if self.connects_to(other):
            return Action(
                pop_char=self.pop_char,
                push_char=other.push_char,
                read_char=None
            )
        return None


    def __lt__(self, other):
        if opt_lt(self.read_char, other.read_char):
            return True
        if self.read_char != self.read_char:
            return False
        if opt_lt(self.pop_char, other.pop_char):
            return True
        if self.pop_char != other.pop_char:
            return False
        return opt_lt(self.push_char, other.push_char)

@dataclass(eq=True, frozen=True)
class Link(Generic[S,A,B]):
    action: Action[A,B]
    dst:Node[S,A]

    def __lt__(self, other):
        return (self.action, self.dst) < (other.action, other.dst)

    def advance(self):
        if self.action.can_advance():
            return self.dst
        else:
            return None

    @classmethod
    def make(cls, src:Node[S,A], action:Action[A,B], dst:S):
        return cls(
            action=action,
            dst=Node(
                state=dst,
                index=src.index + action.get_increment()
            )
        )

@dataclass
class Computation(Generic[S, A, B]):
    input:Collection[A]
    start:Node[S,A]
    steps:Dict[Node[S,A],List[Link[S,A,B]]]
    final:Collection[Node[S,A]]

    def rejects(self):
        return len(self.final) == 0


    def update(self):
        edges = set( (
            (src, link.action, link.dst)
            for (src, links) in self.steps.items()
            for link in links
        ))
        new_edges = set()
        for src in self.steps.keys():
            for link1 in self.steps[src]:
                for link2 in self.steps.get(link1.dst, ()):
                    act = link1.action.add(link2.action)
                    if act is not None:
                        edge = (src, act, link2.dst)
                        if not edge in edges:
                            edges.add(edge)
                            new_edges.add(edge)
        return new_edges

    def next(self):
        new_edges = self.update()
        if len(new_edges) == 0:
            return self
        new_steps = dict(((k,list(vs)) for k, vs in self.steps.items()))
        for (src, act, dst) in new_edges:
            new_steps[src].append(Link(action=act, dst=dst))
        return Computation(
            input = self.input,
            start = self.start,
            final = self.final,
            steps = new_steps,
        )


    def reduce(self):
        g = self
        while True:
            new_g = g.next()
            if new_g is g:
                return g
            g = new_g


    def reaches_final(self):
        to_do = [self.start]
        visited = set(to_do)
        while len(to_do) > 0:
            curr = to_do.pop(0)
            if curr in self.final:
                return True
            for link in self.steps.get(curr, ()):
                dst = link.advance()
                if dst is not None and dst not in visited:
                    to_do.append(dst)
                    visited.add(dst)
        return False


#############################################


class PDA(Generic[S,A,B]):
    start_stack_state : StackState[S,B]
    def __init__(self,
                 states:Optional[Collection[S]]=None,
                 alphabet:Optional[Collection[A]]=None,
                 stack_alphabet:Optional[Collection[B]]=None,
                 transitions:Optional[PTransitionTable[S,A,B]]=None,
                 start_state:Optional[S]=None,
                 accepted_states:Optional[typing.Union[Collection[S], Callable[[S], bool]]]=None):
        assert transitions is not None
        tsx:Dict[InTransition[S,A,B], Tuple[Transition[S,B], ...]] = dict()
        for ((src, c, o), t) in transitions.items():
            tsx[(src, c, o)] = tuple([t]) if isinstance(t, Transition) else tuple(t)
        self.transitions = tsx
        # alphabet
        if alphabet is None:
            alphabet = set((c for (_, c, _) in tsx if c is not None))
        # stack_alphabet
        if stack_alphabet is None:
            stack_alphabet = set((c for (_, _, c) in tsx if c is not None))
        self.stack_alphabet = stack_alphabet
        # states
        if states is None:
            states = set(k for (k, _, _) in tsx)
            states.add(start_state)
            for ts in tsx.values():
                for t in ts:
                    states.add(t.dst)
        assert start_state in states, f"start_state={repr(start_state)} not in states={states}"
        # XXX: we want to remove states at some point
        self.states = states
        # ---
        assert start_state is not None
        assert accepted_states is not None
        self.alphabet = alphabet
        self.start_state = start_state
        self.accepted_states = wrap_accepted_states(accepted_states)
        self.start_stack_state = StackState(state=start_state, stack=())

    def as_graph(self) -> StateDiagram[S,StateDiagram_Edge[A,B]]:
        nodes = set(self.states)
        edges:Dict[Tuple[S,S],Set[StateDiagram_Edge[A,B]]] = dict()
        for (src, c, o), ts in self.transitions.items():
            for t in ts:
                edge = edges.get((src, t.dst), None)
                if edge is None:
                    edges[(src, t.dst)] = edge = set()
                edge.add((c, o, t.push_arg))
        return self.states, edges

    def epsilon(self, states:Iterable[StackState[S,B]]) -> FrozenSet[StackState[S,B]]:
        return epsilon(self.transition_func, states)

    def bounded_epsilon(self, states:Iterable[StackState[S,B]], bound:int, abort=True) -> FrozenSet[StackState[S,B]]:
        states = set(states)
        while True:
            count = len(states)
            states.update(multi_transition(self.transition_func, states, None))
            if count == len(states):
                return frozenset(states)
            state_count = Counter((s.state for s in states))
            (st, total), = state_count.most_common(1)
            if total > bound:
                if abort:
                    raise EpsilonOverflowError(st)
                else:
                    return frozenset(states)

    def multi_transition(self, states:Iterable[StackState[S,B]], char:Optional[A]) -> AbstractSet[StackState[S,B]]:
        return multi_transition(self.transition_func, states, char)



    def get_computation(self, input:Collection[A]) -> Computation[S, A, B]:
        # Build a dictionary of outgoing links
        outgoing:Dict[Tuple[S,Optional[A]],List[Tuple[Action[A,B],S]]] = dict()
        for ((src, read_char, pop_char), tsxs) in self.transitions.items():
            for tsx in tsxs:
                key = (src, read_char)
                out = outgoing.get(key, None)
                if out is None:
                    outgoing[key] = out = []
                act = Action(
                    read_char=read_char,
                    pop_char=pop_char,
                    push_char=tsx.push_arg
                )
                out.append((act, tsx.dst))
        # Compute the outgoing links of a node
        def get_outgoing(src:Node[S,A]):
            result:Set[Edge[S,A,B]] = set()
            if src.index < len(input):
                for (act, dst) in outgoing.get((src.state, input[src.index]), ()):
                    yield Link.make(src, act, dst)
            for (act, dst) in outgoing.get((src.state, None), ()):
                yield Link.make(src, act, dst)
        # Predicate that returns if a node is final
        last_index = len(input)
        def is_final(x):
            return last_index == x.index and self.accepted_states(x.state)

        final:Set[Node[S,A]] = set()
        start = Node.make_start(self, input)
        to_do = [start]
        nodes = set(to_do)
        steps:Dict[Node[S,A],List[Link[S,A,B]]] = dict()
        if is_final(start):
            final.add(start)
        while len(to_do) > 0:
            src = to_do.pop(0)
            for link in get_outgoing(src):
                # Update links
                out = steps.get(src)
                if out is None:
                    steps[src] = out = []
                out.append(link)

                dst = link.dst
                if dst not in nodes:
                    to_do.append(dst)
                    nodes.add(dst)
                    if is_final(dst):
                        final.add(dst)


        return Computation(
            start=start,
            steps=steps,
            final=final,
            input=input
        ) 


    def accepts2(self, word:Collection[A]):
        c = self.get_computation(word)
        if c.rejects():
            return False
        return c.reduce().reaches_final()

    def run(self, word:Collection[A], stack_bound=None, yield_nodes=False, yield_edges=False):
        if stack_bound is None:
            stack_bound = (1 + len(word)) * len(self.states) * len(self.states)
        st = PDANode.make_start(self, word, stack_bound)
        nodes = [st]
        visited = set([st.get_key()])
        while len(nodes) > 0:
            curr = nodes[0]
            key = curr.get_key()
            del nodes[0]
            if yield_nodes:
                yield curr
            for x in curr.get_next(self.transition_func, self.accepted_states, word):
                if yield_edges:
                    yield (curr, x)
                key = x.get_key()
                if key not in visited:
                    nodes.append(x)
                    visited.add(key)

    def accepts(self, word:Collection[A], stack_bound=None) -> bool:
        for node in self.run(word, stack_bound, yield_nodes=True):
            if node.is_final(self, word):
                return True
        return False

    def accepts_unsafe(self, inputs:Collection[A], epsilon_bound=None, abort=True) -> bool:
        do_epsilon = self.epsilon if epsilon_bound is None else lambda x: self.bounded_epsilon(x, epsilon_bound, abort=abort)
        # Perform epsilon transitions of the state
        states:AbstractSet[StackState[S,B]] = do_epsilon({self.start_stack_state})
        for i in inputs:
            if len(states) == 0:
                # Not accepted
                return False
            # Get the transitions and then perform epsilon transitions
            states = do_epsilon(self.multi_transition(states, i))
        return len(set(filter(self.accepted_states,
                              (s.state for s in states)))) > 0



    def transition_func(self, s:StackState[S,B], char:Optional[A]) -> FrozenSet[StackState[S,B]]:
        result:Set[StackState[S,B]] = set()
        for stack_char in cons(None, iter(self.stack_alphabet)):    
            for t in self.transitions.get((s.state, char, stack_char), ()):
                next_st = s.advance(stack_char, t)
                if next_st is not None:
                    result.add(next_st)
        return frozenset(result)

    def accepts_ex(self, inputs):
        derive = DerivationGraph(
            alphabet=self.alphabet,
            start_state=self.start_stack_state,
            transition_func=self.transition_func,
            accepted_states=lambda x: self.accepted_states(x.state)
        )
        return derive.accepts_edges(inputs)

    @staticmethod
    def transitions_from_list(lst:List[Tuple[InTransition[S,A,B],Transition[S,B]]]) -> PTransitionTable[S,A,B]:
        tsx:Dict[InTransition[S,A,B],List[Transition[S,B]]] = dict()
        for (k, v) in lst:
            elems = tsx.get(k, None)
            if elems is None:
                elems = tsx[k] = []
            elems.append(v)
        return tsx
################################################################################

@dataclass(frozen=True, order=True)
class Rule:
    name:str
    body: Tuple[str,...]
    __iter__ = astuple


def indices(elems, match):
    for idx, elem in enumerate(elems):
        if match(elem):
            yield idx


def create_variable(vars):
    counter = 1
    while True:
        var = f"X_{counter}"
        if var not in vars:
            return var
        counter += 1


class CFG:
    def __init__(self, variables, terminals, rules, start):
        self.variables = frozenset(variables)
        self.terminals = frozenset(terminals)
        self.start = start
        # Check well-formedness
        assert len(
            self.terminals.intersection(self.variables)
        ) == 0, f"V interect A={self.terminals.intersection(self.variables)}"
        assert self.start in self.variables, f"start={self.start!r} not in variables={self.variables!r}"
        new_rules = set()
        # Ensure the rules are proper
        for (head, body) in rules:
            assert head in self.variables, f"rule name {head!r} not in variables {self.variables!r}"
            body = tuple(body)
            for elem in body:
                if elem not in self.variables and elem not in self.terminals:
                    raise ValueError(
                        f"Element {repr(elem)} not in variables={repr(self.variables)}, nor in terminals={repr(self.terminals)}"
                    )
            new_rules.add(Rule(head, body))
        new_rules = list(new_rules)
        new_rules.sort()
        self.rules = tuple(new_rules)

    def add_start(self, create_variable=create_variable):
        """
        Add S0 -> S where S was the initial rule and S0 is new.
        """
        new_start = create_variable(self.variables)
        new_variables = set(self.variables)
        new_variables.add(new_start)
        new_rules = list(self.rules)
        new_rules.append(Rule(new_start, self.start))
        return CFG(new_variables, self.terminals, new_rules, new_start)

    def get_empty_rules(self):
        return (var for var, body in self.rules if len(body) == 0)

    def remove_empty(self):
        """
        Remove all S -> [].
        For every S removed and for every occurrence R -> uSv add rule R -> uv.
        """
        empty_vars = set(self.get_empty_rules())
        if len(empty_vars) == 0:
            # Nothing to remove
            return self
        assert self.start not in empty_vars
        new_rules = []
        for r in self.rules:
            if len(r.body) == 0:
                continue
            new_rules.append(r)
            to_visit = [r]
            while len(to_visit) > 0:
                elem = to_visit.pop()
                for idx in indices(elem.body, lambda x: x in empty_vars):
                    new_body = elem.body[:idx] + elem.body[idx + 1:]
                    if len(new_body) > 0:
                        new_rule = Rule(r.name, new_body)
                        to_visit.append(new_rule)
                        new_rules.append(new_rule)
        return CFG(variables=self.variables,
                   terminals=self.terminals,
                   rules=new_rules,
                   start=self.start)

    def get_self_rules(self):
        return (x.name for x in self.rules if (x.name, ) == x.body)

    def is_unit_rule(self, rule):
        return len(rule.body) == 1 and rule.body[0] in self.variables

    def comp_unit_pairs(self):
        # 1. Get the initial rules
        pairs = filter(self.is_unit_rule, self.rules)
        # 2. Convert rules into a map
        units = dict()
        for r in pairs:
            out = units.get(r.name, None)
            if out is None:
                units[r.name] = out = set()
            out.add(r.body[0])
        # 3. Compute the transitive closure for each rule
        added = True
        while added:
            added = False
            for x, outs in units.items():
                for y in tuple(outs):
                    for z in units.get(y, ()):
                        if z not in outs:
                            outs.add(z)
                            added = True
        return units

    def add_terminal_rules(self, create_variable=create_variable):
        """
        Only the following rules can be replaced: A -> cB, A -> Bc, A -> cc
        """
        terminals = set(c for r in self.rules for c in r.body
                        if c in self.terminals)
        new_variables = set(self.get_nonterminal_vars())
        term_to_var = dict()
        new_rules = []

        for t in terminals:
            new_var = create_variable(new_variables)
            term_to_var[t] = new_var
            new_variables.add(new_var)
            new_rules.append(Rule(new_var, (t, )))

        old_vars = dict()
        for r in self.rules:
            if r.name not in new_variables:
                t, = r.body
                old_vars[r.name] = term_to_var[t]

        for r in self.rules:
            if len(r.body) == 0:
                raise ValueError(
                    f"Call `remove_empty()` first. Error: found {r}")
            if len(r.body) > 2:
                raise ValueError(
                    f"Call `make_binary()` first. Error: found {r}")
            if len(r.body) == 1:
                new_rules.append(r)
            else:
                assert len(r.body) == 2
                # Replace old variables by new variables, terminals by new variables
                new_body = tuple(
                    (term_to_var.get(x, old_vars.get(x, x)) for x in r.body))
                new_rules.append(Rule(r.name, new_body))
        new_start = old_vars.get(self.start, self.start)
        return CFG(new_variables, self.terminals, new_rules, new_start)

    def remove_unit_rules(self):
        """
        Remove all rules A->B.
        For every A->B removed add A->s if B->s and A->s not removed.
        """
        units = self.comp_unit_pairs()
        # 1. Remove unit rules
        new_rules = set()
        for r in self.rules:
            if not (self.is_unit_rule(r)
                    and r.body[0] in units.get(r.name, ())):
                new_rules.add(r)
        # 2. Add replace
        for x, outs in units.items():
            for a in outs:
                for w in self[a]:
                    # Ensure the new rule is not in removed
                    if len(w) == 1 and w[0] in outs:
                        continue
                    new_rules.add(Rule(x, w))
        return CFG(self.variables, self.terminals, new_rules, self.start)

    def make_binary_step(self, create_variable=create_variable):
        if all(len(r.body) <= 2 for r in self.rules):
            return self

        new_variables = set(self.variables)
        new_rules = []
        for r in self.rules:
            if len(r.body) > 2:
                head, *rest = r.body
                var = create_variable(new_variables)
                new_variables.add(var)
                new_rules.append(Rule(r.name, (head, var)))
                new_rules.append(Rule(var, rest))
            else:
                new_rules.append(r)
        return CFG(new_variables, self.terminals, new_rules, self.start)

    def make_binary(self, create_variable=create_variable):
        cfg = self
        new_cfg = self.make_binary_step()
        while cfg is not new_cfg:
            cfg, new_cfg = new_cfg, new_cfg.make_binary_step(create_variable)
        # Finally, remove all terminals
        return cfg

    def to_chomsky_normal_form(self):
        return self.add_start()\
            .remove_empty()\
            .remove_unit_rules()\
            .make_binary()\
            .add_terminal_rules()

    def get_nonterminal_vars(self):
        for var, prods in self.items():
            prods = tuple(prods)
            if len(prods) == 1:
                prod, = prods
                if len(prod) == 1 and prod[0] in self.terminals:
                    continue

            yield var

    def map_terminal_to_vars(self):
        result = dict()
        for r in self.rules:
            if len(r.body) != 1:
                continue
            char = r.body[0]
            if char not in self.terminals:
                continue
            data = result.get(char, None)
            if data is None:
                result[char] = data = set()
            data.add(r.name)
        return result

    def cyk_match(self, word):
        terms = self.map_terminal_to_vars()
        chart = dict()
        N = len(word)
        for s in range(1, N + 1):
            letter = word[s - 1]  # a_s
            result = terms.get(letter, None)  # forall R: R -> a_s \in G
            if result is not None:
                chart[(1, s)] = result
        # keep only binary rules
        binary = list(r for r in self.rules if len(r.body) == 2)
        for l in range(2, N + 1):
            for s in range(1, N - l + 1 + 1):
                for p in range(1, l - 1 + 1):
                    for rule in binary:
                        (left, right) = rule.body
                        if left in chart.get(
                            (p, s), ()) and right in chart.get((l - p, s + p),
                                                               ()):
                            idx = (l, s)
                            result = chart.get(idx, None)
                            if result is None:
                                chart[idx] = result = set()
                            result.add(rule.name)
        return chart

    def accepts(self, word):
        chart = self.cyk_match(word)
        return len(chart.get((len(word), 1), ())) > 0

    def __getitem__(self, key):
        assert key in self.variables, f"{repr(key)} not in {self.variables}"
        return (body for head, body in self.rules if head == key)

    def __iter__(self):
        return iter(self.variables)

    def items(self):
        groups = groupby(self.rules, itemgetter(0))
        return ((k, map(itemgetter(1), g)) for k, g in groups)

    def __repr__(self):
        return f"CFG(variables={repr(self.variables)}, terminals={repr(self.terminals)}, rules={self.rules}, start={repr(self.start)})"

    def as_tex(self):
        for variable in sorted(self.variables):
            rules = map(lambda x: " ".join(map(str, x)), self[variable])
            print(variable + " &\\rightarrow " + " \\mid ".join(rules) +
                  " \\\\")


def find_vars(variables, step):
    return (idx for idx, letter in enumerate(step) if letter in variables)


def apply_rules(step, index, productions):
    """
    >>> list(apply_rules([0, 1, 'A', 2], 2, [[], ['X', 'X']]))
    [(0, 1, 2), (0, 1, 'X', 'X', 2)]
    """
    start = tuple(step[0:index])
    end = tuple(step[index + 1:])
    for prod in productions:
        yield start + tuple(prod) + end


class UnknownProductionError(Exception):
    def __init__(self, variable, index, string):
        self.variable = variable
        self.index = index
        self.string = string


class MismatchError(Exception):
    def __init__(self, index, variable, candidates):
        self.index = index
        self.variable = variable
        self.candidates = candidates


def check_step(cfg, step, next_step):
    """
    >>> cfg = CFG(
    ...   variables = ['C'],
    ...   terminals = ('{', '}'),
    ...   rules = [
    ...     Rule('C', ['{', 'C', '}']),
    ...     Rule('C', ['C', 'C']),
    ...     Rule('C', [])
    ...   ],
    ...   start = 'C'
    ... )
    >>> check_step(cfg, ('C',), ('C', 'C'))
    >>> check_step(cfg, ('C', 'C'), ('{', 'C', '}', 'C'))
    >>> check_step(cfg, ('C',), ('{', 'C', '}', 'C'))
    Traceback (most recent call last):
            ...
    MismatchError: (('C',), ('{', 'C', '}', 'C'), [0])
    >>> check_step(cfg, ('C', 'C',), ('C', 'X'))
    Traceback (most recent call last):
            ...
    MismatchError: (('C', 'C'), ('C', 'X'), [0, 1])
    """
    for index in find_vars(cfg.variables, step):
        #index = find_diff(step, next_step)
        var = step[index]
        prods = list(cfg[var])
        if len(prods) == 0:
            raise UnknownProductionError(var, index, step)
        candidates = list(apply_rules(step, index, prods))
        for candidate in candidates:
            if candidate == next_step:
                return

    raise MismatchError(step, next_step, list(find_vars(cfg.variables, step)))


def iter_steps(derivation):
    steps = list(map(tuple, derivation))
    next_steps = list(steps)
    next_steps.append(None)
    del next_steps[0]
    for idx, (step, next_step) in enumerate(zip(steps, next_steps)):
        if next_step is None:
            assert idx == len(steps) - 1
            return
        yield (step, next_step)


Atom = namedtuple('Atom', ["data"])
Subst = namedtuple('Subst', ["variable", "data"])


def subst(variable, *args):
    return Subst(variable, args)


def node_letter(node):
    assert isinstance(node, Atom) or isinstance(node, Subst), f"{repr(node)}"
    if isinstance(node, Atom):
        return node.data
    return node.variable


def subst_match(node, rule):
    """
    >>> subst_match(Subst('E', [Atom('E'), Atom('-'), Atom('E')]), "E-E")
    True
    """
    if len(rule) != len(node.data):
        return False

    return all(x == y for (x, y) in zip(rule, map(node_letter, node.data)))


def node_match(cfg, node):
    if isinstance(node, Atom):
        assert node.data in cfg.terminals or node.data in cfg.variables, f"{repr(node.data)} not in {cfg.terminals} + {cfg.variables}"
        return True
    for prod in cfg[node.variable]:
        if subst_match(node, prod):
            return True
    return False


def check_parse_tree(cfg, node):
    """
    >>> cfg = CFG(
    ...   variables = ['E'],
    ...   terminals = ('N', '+', '-'),
    ...   rules = [
    ...     Rule('E', "E+E"),
    ...     Rule('E', "E-E"),
    ...     Rule('E', "N")
    ...   ],
    ...   start = 'E'
    ... )
    >>> check_parse_tree(cfg,
    ...  Subst('E',
    ...     [
    ...        Subst('E', [Atom('E'), Atom('-'), Atom('E')]),
    ...        Atom('+'),
    ...        Atom('E')
    ...     ]
    ...   )
    ... )
    """
    assert node_match(cfg, node), node
    if isinstance(node, Subst):
        for child in node.data:
            check_parse_tree(cfg, child)


def trace_step(cfg, step, next_step):
    for index in find_vars(cfg.variables, step):
        var = step[index]
        for idx, candidate in enumerate(apply_rules(step, index, cfg[var])):
            if candidate == next_step:
                return (index, var, idx)


def trace_derivation(cfg, derivation):
    for (curr_step, next_step) in iter_steps(derivation):
        yield trace_step(cfg, curr_step, next_step)

def dfa_to_cfg(dfa, state_to_var=lambda x: x):
    nodes, edges = dfa.as_graph()
    variables = set()
    terminals = set()
    rules = []
    start = state_to_var(dfa.start_state)
    for q in dfa.end_states:
        rules.append(Rule(state_to_var(q), []))
    for ((src, dst), elems) in edges.items():
        src = state_to_var(src)
        dst = state_to_var(dst)
        variables.add(src)
        variables.add(dst)
        for char in elems:
            terminals.add(char)
            rules.append(Rule(src, [char, dst]))
    return CFG(variables, terminals, rules, start)


class DerivationError(Exception):
    def __init__(self, index, error):
        self.index = index
        self.error = error


def check_derivation(cfg, steps):
    """
    >>> cfg = CFG(
    ...   variables = ['C'],
    ...   terminals = ('{', '}'),
    ...   rules = [
    ...     Rule('C', ['{', 'C', '}']),
    ...     Rule('C', ['C', 'C']),
    ...     Rule('C', [])
    ...   ],
    ...   start = 'C'
    ... )
    >>> check_derivation(cfg, [
    ...   'C',
    ...   'CC',
    ...   '{C}C',
    ...   '{C}{C}',
    ...   '{{C}}{C}',
    ...   '{{}}{C}',
    ...   '{{}}{}'
    ... ])
    >>> check_derivation(cfg, [
    ...   'C',
    ...   '{',
    ...   '{C}C',
    ...   '{C}{C}',
    ... ])
    Traceback (most recent call last):
            ...
    DerivationError: (MismatchError(('C',), ('{',), [0]), 0)
    """
    for idx, (step, next_step) in enumerate(iter_steps(steps)):
        try:
            check_step(cfg, step, next_step)
        except (MismatchError, UnknownProductionError) as e:
            raise DerivationError(e, idx)

PEdge = Tuple[int,Tuple[Optional[str], Optional[str]], int]

def get_edges(curr:int, q_loop:int, r:Rule) -> Tuple[int,List[PEdge]] :
    body = list(r.body)
    dst = q_loop
    edges:List[PEdge] = []
    char:Optional[str] = None
    if len(body) > 0:
        char = body[-1]
        del body[-1]
        for elem in body:
            edges.append((curr, (None, elem), dst))
            dst = curr
            curr += 1
    else:
        char = None
    edges.append((q_loop, (r.name, char), dst))
    edges.reverse()
    return curr, edges

PKey = InTransition[int, str, str]

def cfg_to_pda(c:CFG, Empty:str) -> PDA[int,str,str]:
    q_init = 0
    q_start = 1
    q_loop = 2
    q_accept = 3
    states = {q_init, q_start, q_loop, q_accept}
    curr = 4
    tsx:List[Tuple[PKey, Transition]] = []
    tsx.append(((q_init, None, None), Transition(Empty, q_start)))
    tsx.append(((q_start, None, None), Transition(c.start, q_loop)))
    tsx.append(((q_loop, None, Empty), Transition(None, q_accept)))
    for r in c.rules:
        curr, edges = get_edges(curr, q_loop, r)
        for (src, (pop, push), dst) in edges:
            tsx.append(((src, None, pop), Transition(push, dst)))
    for t in c.terminals:
        tsx.append(((q_loop, t, t), Transition(None, q_loop)))
    return PDA(
        start_state=q_init,
        transitions=PDA.transitions_from_list(tsx),
        accepted_states=[q_accept],
    )
