# Karakuri: a library for handling automata theory data structures

Karakuri includes algorithms related to regular languages: finite-state machines (FSM),
deterministic automata (DFA), non-deterministic automata (NFA), regular
expressions (REGEX).

### Features
* Vast library of algorithms that operate on automata theory
* Visualization of beautiful diagrams, powered by Graphiviz and Tikz
* Python type hints for all data structures and functions
* Optimized for low-memory usage

### Educational features
* Explainable algorithms through visualization (eg, by stepping through each algorithm)
* Closely follows the theoretical formalism

## Regular languages

What we have implemented for regular languages:
* NFA/DFA acceptance
* NFA to DFA
* DFA to NFA
* NFA to REGEX
* REGEX to NFA
* language equivalence
* emptiness test
* DFA minimization
* shuffling NFAs
* stepping through acceptance of NFAs
* stepping through acceptance of DFAs
* visualizing NFAs, DFAs
* visualizing acceptance/rejecting words
* Language operators for NFAs include: concatenation, union, nil, void
  (emptyset), Kleene star
* Generalized NFAs
* Stepping through conversion from REGEX to NFA