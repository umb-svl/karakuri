from karakuri.regular import *
from karakuri.hml import *

def test_acts():
    alpha = set("abc")
    assert AIntersection(
        ANot(AAtom("a")),
        ANot(AAtom("b")),
    ).interpret(alpha) == frozenset(["c"])

def coerce(l, alphabet):
    if isinstance(l, Formula):
        return coerce(l.interpret(alphabet), alphabet)
    if isinstance(l, Regex):
        return coerce(regex_to_nfa(l, alphabet), alphabet)
    if isinstance(l, NFA):
        return nfa_to_dfa(l)
    assert isinstance(l, DFA)
    return l

def do_equals(l, r, alphabet):
    assert coerce(l, alphabet).is_equivalent_to(coerce(r, alphabet))

AB = set(["a", "b"])
A = AAtom("a")
B = AAtom("b")
ALL_AB = Star(Union(Char("a"), Char("b")))


def test_true():
    formula = F_TRUE
    do_equals(F_TRUE, ALL_AB, alphabet=AB)

def test_false():
    formula = F_FALSE
    do_equals(F_FALSE, VOID, alphabet=AB)

def test_and():
    do_equals(F_FALSE, FAnd(F_FALSE, F_TRUE), alphabet=AB)
    do_equals(F_FALSE, FAnd(F_TRUE, F_FALSE), alphabet=AB)
    do_equals(F_FALSE, FAnd(F_FALSE, F_FALSE), alphabet=AB)
    do_equals(F_TRUE, FAnd(F_TRUE, F_TRUE), alphabet=AB)

def test_or():
    do_equals(F_TRUE, FOr(F_FALSE, F_TRUE), alphabet=AB)
    do_equals(F_TRUE, FOr(F_TRUE, F_FALSE), alphabet=AB)
    do_equals(F_FALSE, FOr(F_FALSE, F_FALSE), alphabet=AB)
    do_equals(F_TRUE, FOr(F_TRUE, F_TRUE), alphabet=AB)

def test_not():
    do_equals(F_FALSE, FNot(F_TRUE), alphabet=AB)
    do_equals(F_TRUE, FNot(F_FALSE), alphabet=AB)
    do_equals(F_TRUE, FNot(FNot(F_TRUE)), alphabet=AB)
    do_equals(F_FALSE, FNot(FNot(F_FALSE)), alphabet=AB)
    do_equals(F_FALSE, FNot(FOr(F_TRUE, F_FALSE)), alphabet=AB)


def test_possibly():
    do_equals(FPossibly(A, F_TRUE), Concat(Char("a"), ALL_AB),
        alphabet=AB)
    do_equals(FPossibly(B, F_TRUE), Concat(Char("b"), ALL_AB),
        alphabet=AB)
    do_equals(FNot(FPossibly(A, F_TRUE)),
        Union(Concat(Char("b"), ALL_AB), NIL), alphabet=AB)
    do_equals(FPossibly(A, F_FALSE), F_FALSE, alphabet=AB)
    do_equals(FNecessarily(A, F_TRUE), F_TRUE, alphabet=AB)


def test_coffee_machine():
    alpha = set(["coin", "good", "bad"])
    formula = FPossibly(AAtom("coin"),
        FPossibly(AAtom("good"), F_TRUE)
    )

    any_char = Union(
        Union(Char("coin"), Char("good")),
        Char("bad"))
    all_strings = Star(any_char)
    expected = Concat(
        Concat(Char("coin"), Char("good")),
        all_strings
    )
    do_equals(formula, expected, alphabet=alpha)


def test_serialize_action():
    exp = AIntersection(
        AUnion(A_ALL, A_EMPTY),
        AAtom("foo"),
    )
    assert Action.deserialize(exp.serialize()) == exp

def test_serialize_formula_1():
    form = FOr(
        FAnd(F_TRUE, F_FALSE),
        F_TRUE)
    assert Formula.deserialize(form.serialize()) == form

def test_serialize_formula_2():
    form = FPossibly(AAtom("bar"), F_TRUE)
    assert Formula.deserialize(form.serialize()) == form
    form = FPossibly(AAtom("foo"),
            FPossibly(AAtom("bar"),
            FPossibly(AAtom("baz"), F_TRUE)))
    assert Formula.deserialize(form.serialize()) == form

def test_serialize_formula_3():
    form = FNecessarily(AAtom("bar"), F_TRUE)
    assert Formula.deserialize(form.serialize()) == form
    form = FNecessarily(AAtom("foo"),
            FNecessarily(AAtom("bar"),
            FNecessarily(AAtom("baz"), F_TRUE)))
    assert Formula.deserialize(form.serialize()) == form
