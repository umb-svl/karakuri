
from karakuri.regular import NFA, DFA, nfa_to_regex, regex_to_nfa, Union, \
        Char, Concat, concat, shuffle as And, nfa_to_dfa, Regex, dfa_to_nfa, \
        nfa_to_regex, GNFA, NIL, Star, VOID, star, concat, union
from typing import Dict

def test_fig1_2():
    # Fig 1.2 of the ITC Sipser book
    State = "open", "close"
    Input = "neither", "front", "rear", "both"
    def state_transition(old_st, i):
        if old_st == "close" and i == "front": return "open"
        if old_st == "open" and i == "neither": return "close"
        return old_st

    a = DFA(Input, state_transition, "close", ["close"])
    assert a.accepts(["front", "neither"])
    assert not a.accepts(["front"])
    assert not a.accepts(["front", "front"])
    assert a.get_shortest_string() == tuple()

def test_shortest_string():
    a = DFA(alphabet=[0, 1],
        transition_func=DFA.transition_table({
            ("q_even", 0): "q_even",
            ("q_even", 1): "q_odd",
            ("q_odd", 0): "q_odd",
            ("q_odd", 1): "q_even",
        }),
        start_state="q_even",
        accepted_states=["q_odd"],
    )
    assert a.get_shortest_string() == tuple([1])
    a = DFA(alphabet=[0,1],
        transition_func=DFA.transition_table({
            ("q", 0): "q_0",
            ("q", 1): "q",
            ("q_0", 0): "q_00",
            ("q_0", 1): "q",
            ("q_00", 0): "q_00",
            ("q_00", 1): "q_001",
            ("q_001", 0): "q_001",
            ("q_001", 1): "q_001",
        }),
        start_state="q",
        accepted_states=["q_001"],
    )
    assert a.get_shortest_string() == (0,0,1)

def test_divergence_index():
    # Accepts a series of 0s
    a = DFA(alphabet=[0, 1],
        transition_func=DFA.transition_table({
            ("q_1", 0): "q_1",
            ("q_1", 1): "q_2",
            ("q_2", 0): "q_2",
            ("q_2", 1): "q_2",
        }),
        start_state="q_1",
        accepted_states=["q_1"],
    )
    assert a.get_divergence_index([]) is None
    assert a.get_divergence_index([1]) == 0
    assert a.get_divergence_index([0,1]) == 1
    assert a.get_divergence_index([0,0,1]) == 2
    assert a.get_divergence_index([0,0,0,1]) == 3
    assert a.get_divergence_index([0,0,0,1,1]) == 3

def test_fig1_4():
    def state_transition(src, i):
        if src == 1 and i == 0:
            return 1
        elif src == 1 and i == 1:
            return 2
        elif src == 2 and i == 0:
            return 3
        elif src == 2 and i == 1:
            return 2
        elif src == 3:
            return 2
    a = DFA([0,1], state_transition, 1, [2])
    for word in [[1], [0,1], [1,1], [0,1,0,1,0,1,0,1,0,1], [1,0,0], [0,1,0,0]]:
        assert a.accepts(word)
    for word in [[0], [1,0], [1,0,1,0,0,0]]:
        assert not a.accepts(word)
    # Alternative definition of a DFA, by giving the transition table
    a = DFA([0,1], DFA.transition_table({
        (1,0): 1,
        (1,1): 2,
        (2,0): 3,
        (2,1): 2,
        (3,0): 2,
        (3,1): 2,
    }), 1, [2])
    for word in [[1], [0,1], [1,1], [0,1,0,1,0,1,0,1,0,1], [1,0,0], [0,1,0,0]]:
        assert a.accepts(word)
    for word in [[0], [1,0], [1,0,1,0,0,0]]:
        assert not a.accepts(word)


B_P = "b.pressed"
B_R = "b.release"


def create_button():
    return NFA(
        # states=[0, 1],
        alphabet=[B_P, B_R],
        transition_func=NFA.transition_edges([
            (0, [B_P], 1),
            (1, [B_R], 0),
        ]),
        start_state=0,
        accepted_states=[0, 1],
    )


LA_ON = "la.on"
LA_OFF = "la.off"


def create_led_a():
    return NFA(
        # states=[0, 1],
        alphabet=[LA_ON, LA_OFF],
        transition_func=NFA.transition_edges([
            (0, [LA_ON], 1),
            (1, [LA_OFF], 0),
        ]),
        start_state=0,
        accepted_states=[0, 1],
    )


LB_ON = "lb.on"
LB_OFF = "lb.off"


def create_led_b():
    return NFA(
        # states=[0, 1],
        alphabet=[LB_ON, LB_OFF],
        transition_func=NFA.transition_edges([
            (0, [LB_ON], 1),
            (1, [LB_OFF], 0),
        ]),
        start_state=0,
        accepted_states=[0, 1],
    )


T_T = "t.t"
T_C = "t.c"
T_S = "t.s"


def create_timer():
    return NFA(
        # states=[0, 1],
        alphabet=[T_T, T_C, T_S],
        transition_func=NFA.transition_edges([
            (0, [T_S], 1),
            (1, [T_C, T_T], 0),
        ]),
        start_state=0,
        accepted_states=[0, 1],
    )


LEVEL1 = "l1"
LEVEL2 = "l2"
STANDBY1 = "s1"
STANDBY2 = "s2"
HELLO_WORLD_TRIGGERS : Dict[str,Regex[str]] = {
    LEVEL1:
        Concat.from_list(map(Char[str], [B_P, B_R, LA_ON, T_S])),
    LEVEL2:
        Concat.from_list([
            Char(B_P),
            Char(B_R),
            And(Char(T_C), Char(LB_ON)),
            Char(T_S),
        ]),
    STANDBY1:
        concat(Char(T_T), Char(LA_OFF)),
    STANDBY2:
        concat(
            Union(Concat.from_list(map(Char, [B_P, B_R, T_C])), Char(T_T)),
            And(Char(LB_OFF), Char(LA_OFF)),
        ),
}


def create_hello_world():
    return NFA(
        alphabet=[LEVEL1, LEVEL2, STANDBY1, STANDBY2],
        transition_func=NFA.transition_edges_split([
            (0, LEVEL1, 1),
            (1, LEVEL2, 2),
            (1, STANDBY1, 0),
            (2, STANDBY2, 0),
        ]),
        start_state=0,
        accepted_states=lambda x: 0 <= x <= 2,
    )


def create_led_and_button():
    """
    This example should be a sub-behavior of shuffling button with led-a.
    
    (0) LEDA.ON --->  (1)  BTN.PRS ----> (2)
        <--- LEDA.OFF      <--- BTN.REL
    """
    return NFA(
        alphabet=[LA_ON, LA_OFF, B_P, B_R],
        transition_func=NFA.transition_edges([
            (0, [LA_ON], 1),
            (1, [LA_OFF], 0),
            (1, [B_P], 2),
            (2, [B_R], 1),
        ]),
        start_state=0,
        accepted_states=[0, 1, 2],
    )


def test_button():
    button = create_button()
    assert button.accepts([])
    assert button.accepts([B_P, B_R])
    assert button.accepts([B_P])
    assert not button.accepts([B_R])


def test_convert_dfa():
    button = create_button()
    button = nfa_to_dfa(button)
    assert button.accepts([])
    assert button.accepts([B_P, B_R])
    assert button.accepts([B_P])
    assert not button.accepts([B_R])


def test_complement():
    """
    A simple test on the complement of a DFA
    """
    button = create_button()
    button = nfa_to_dfa(button)
    button = button.complement()
    assert not button.accepts([])
    assert not button.accepts([B_P, B_R])
    assert not button.accepts([B_P])
    assert button.accepts([B_R])


def test_shuffle():
    button = create_button()
    led_a = create_led_a()
    both = button.shuffle(led_a)
    # Both should accept all behaviors of the button
    assert both.accepts([])
    assert both.accepts([B_P, B_R])
    assert both.accepts([B_P])
    assert not both.accepts([B_R])
    # It should also accept all behaviors of led
    assert both.accepts([LA_ON, LA_OFF])
    assert both.accepts([LA_ON])
    assert not both.accepts([LA_OFF])
    # Finally, it should accept interleaving of LED and Button:
    assert both.accepts([LA_ON, B_P, B_R, LA_OFF, B_P])
    # Here we fail because we have B_P followed by B_P
    assert not both.accepts([LA_ON, B_P, LA_OFF, B_P])


def test_shuffle_2():
    n1 = NFA(
        alphabet=['a', 'b'],
        transition_func=NFA.transition_edges_split([
            (0, 'a', 1),
            (1, 'b', 2),
            (2, 'a', 1),
        ]),
        start_state=0,
        accepted_states=[1,2],
    )
    n2 = NFA(
        alphabet=['c', 'd'],
        transition_func=NFA.transition_edges_split([
            (0, 'c', 1),
            (1, 'd', 2),
            (2, 'c', 1),
        ]),
        start_state=0,
        accepted_states=[1,2],
    )
    both = n1.shuffle(n2)
    print(both)
    # Both should accept all behaviors of the button
    assert not both.accepts([])
    assert both.accepts("a")
    assert both.accepts("c")
    assert both.accepts("ac")
    assert both.accepts("ca")
    assert not both.accepts("dca")

def test_sink():
    """
    Non-trivial sink state that is involved in a cycle of sink-states.
    """
    n1 = NFA(
        alphabet=['a', 'b'],
        transition_func=NFA.transition_edges_split([
            (0, 'a', 1),
            (1, 'b', 2),
            (2, 'a', 3),
            (3, 'a', 4),
            (4, 'a', 3),
            (0, 'a', 5),
            (5, 'a', 5),
        ]),
        start_state=0,
        accepted_states=[1,2],
    )
    assert n1.is_sink(5) # easy self-loop
    assert n1.is_sink(3)
    assert n1.is_sink(4)


def test_led_and_button():
    both = create_led_and_button()
    # Both should accept all behaviors of the button
    assert both.accepts([])
    assert both.accepts([LA_ON])
    assert both.accepts([LA_ON, LA_OFF])
    # It should also accept all behaviors of led
    assert both.accepts([LA_ON, B_P])
    assert both.accepts([LA_ON, B_P, B_R, LA_OFF])
    assert both.accepts([LA_ON, B_P, B_R, LA_OFF, LA_ON])
    # Here we fail because we have B_P followed by B_P
    assert not both.accepts([B_R])
    assert not both.accepts([B_P])
    assert not both.accepts([LA_ON, B_R])
    assert not both.accepts([LA_ON, B_P, LA_OFF])


def test_contains():
    button = create_button()
    led_a = create_led_a()
    # All behaviors of button and leda
    both = button.shuffle(led_a)
    large = nfa_to_dfa(both)
    # We describe a behavior that is smaller than the shuffling of all
    behavior = create_led_and_button()
    small = nfa_to_dfa(behavior)
    err = small.subtract(large).minimize()
    assert large.contains(small)

def test_hello_world_not_empty():
    hw = create_hello_world()
    hw_d = nfa_to_dfa(hw)
    hw_s = hw_d.flatten(minimize=True)
    hw_r = nfa_to_regex(hw)
    assert not hw.is_empty()
    assert not hw_d.is_empty()
    assert not hw_s.is_empty()
    # We know that it should not be NIL, it should accept more
    assert nfa_to_regex(hw) != NIL
    assert nfa_to_regex(hw) != VOID

def assert_gnfa(before, after):
    states = set((st for kv in before for st in kv))
    alphabet = set(before.values())
    gn = GNFA(
        states = states,
        alphabet = alphabet,
        transitions = dict(((src,dst), Char(char)) for ((src,dst),char) in before.items()),
        start_state = 0,
        end_state=2,
    )
    _, next_gn = gn.step(node=1)
    assert next_gn.transitions == after

def test_gnfa_step1():
    before = {
        (0, 1): "a",
        (1, 2): "b",
    }
    after = {
        (0,2): Concat(Char("a"), Char("b"))
    }
    assert_gnfa(before, after)

def test_gnfa_step2():
    before = {
        (0,1): "a",
        (1,2): "b",
        (0,3): "c",
        (3,1): "d",
    }
    after = {
        (0,2): Concat(Char("a"), Char("b")),
        (0,3): Char("c"),
        (3,2): Concat(Char("d"), Char("b")),
    }
    assert_gnfa(before, after)

def test_gnfa_step3():
    before = {
        (1,2): "b",
        (0,3): "c",
        (3,1): "d",
        (3,2): "e",
    }
    after = {
        (0,3): Char("c"),
        (3,2): Union(Concat(Char("d"), Char("b")), Char("e")),
    }
    assert_gnfa(before, after)

def test_gnfa_step4():
    """
    Self-loop
    """
    before = {
        (0, 1): "a",
        (1, 2): "b",
        (1, 1): "c",
    }
    after = {
        (0,2): Concat(Char("a"), Concat(Star(Char("c")), Char("b")))
    }
    assert_gnfa(before, after)

def test_gnfa_step5():
    before = {
        (0, 1): "a",
        (1, 2): "b",
        (1, 0): "c",
    }
    after = {
        (0, 0): Concat(Char("a"), Char("c")),
        (0, 2): Concat(Char("a"), Char("b")),
    }
    assert_gnfa(before, after)

def test_nfa_concat():
    alpha = ["a", "b"]
    a1 = NFA(alphabet=alpha,
        transition_func=NFA.transition_table({
            (1, "a"): [1, 2, 3],
            (1, None): [3],
            (2, "b"): [2],
            (2, "a"): [1, 3],
            (3, None): [1],
            (3, "b"): [4],
        }),
        start_state=1,
        accepted_states=[2, 4]
    )
    a2 = NFA(
        alphabet=alpha,
        transition_func=NFA.transition_table({
            (0, None): [1],
            (1, "a"): [2],
            (2, None): [3],
            (3, "b"): [4],
            (4, None): [0],
        }),
        start_state=0,
        accepted_states=[0],
    )
    expected = NFA(alphabet=alpha,
        transition_func=NFA.transition_table({
            (1, "a"): [1, 2, 3],
            (1, None): [3],
            (2, "b"): [2],
            (2, "a"): [1, 3],
            (2, None): [5],
            (3, None): [1],
            (3, "b"): [4],
            (4, None): [5],
            (5+0, None): [5+1],
            (5+1, "a"): [5+2],
            (5+2, None): [5+3],
            (5+3, "b"): [5+4],
            (5+4, None): [5+0],
        }),
        start_state=1,
        accepted_states=[5+0]
    )
    print("EXPECTED:", expected.flatten())
    print("GIVEN:", a1.concat(a2).flatten())
    assert a1.concat(a2) == expected

def test_filter_char():
    """
    Filter character example.
    """
    given = NFA(
        alphabet=["a", "b", "c", "d"],
        transition_func=NFA.transition_table({
            (0, "a"): [1],
            (1, "b"): [2],
            (2, "c"): [3],
            (3, "d"): [4],
        }),
        start_state=0,
        accepted_states=[4],
    ).filter_char(lambda x: x == "a" or x == "b")
    expected = NFA(
        alphabet=["a", "b"],
        transition_func=NFA.transition_table({
            (0, "a"): [1],
            (1, "b"): [2],
            (2, None): [3],
            (3, None): [4],
        }),
        start_state=0,
        accepted_states=[4],
    )
    assert given == expected


def test_eq_1():
    """
    Show that the order of the alphabet has no impact in equality
    """
    a1 = NFA(
        alphabet=["a", "b"],
        transition_func=NFA.transition_table({
            (0, None): [1],
            (1, "a"): [2],
            (2, None): [3],
            (3, "b"): [4],
            (4, None): [0],
        }),
        start_state=0,
        accepted_states=[0],
    )
    a2 = NFA(
        alphabet=["b", "a"],
        transition_func=NFA.transition_table({
            (0, None): [1],
            (1, "a"): [2],
            (2, None): [3],
            (3, "b"): [4],
            (4, None): [0],
        }),
        start_state=0,
        accepted_states=[0],
    )
    assert a1 == a2

def test_nfa_star():
    alpha = ["a", "b"]
    a1 = NFA(
        alphabet=alpha,
        transition_func=NFA.transition_table({
            (1, "a"): [2],
            (2, None): [3],
            (3, "b"): [4],
        }),
        start_state=1,
        accepted_states=[4],
    )
    expected = NFA(
        alphabet=alpha,
        transition_func=NFA.transition_table({
            (0, None): [1],
            (1, "a"): [2],
            (2, None): [3],
            (3, "b"): [4],
            (4, None): [0],
        }),
        start_state=0,
        accepted_states=[0],
    )
    assert a1.star() == expected

def test_step_algo():
    r1 = Char('d')
    r2 = VOID
    r3 = Char('b')
    r4 = Char('e')
    assert union(concat(r1, concat(star(r2), r3)), r4) == \
            Union(Concat(r1, r3), r4)

    r1 = Char('a')
    r2 = Char('b')
    r3 = Char('c')
    r4 = Char('d')
    assert union(concat(r1, concat(star(r2), r3)), r4) == \
            Union(Concat(r1, Concat(Star(r2), r3)), r4)

    r1 = VOID
    r2 = Char('b')
    r3 = Char('c')
    r4 = Char('d')
    assert union(concat(r1, concat(star(r2), r3)), r4) == r4

    r1 = VOID
    r2 = Char('b')
    r3 = VOID
    r4 = Char('d')
    assert union(concat(r1, concat(star(r2), r3)), r4) == r4

    r1 = Char('a')
    r2 = VOID
    r3 = Char('b')
    r4 = VOID
    assert union(concat(r1, concat(star(r2), r3)), r4) == \
            Concat(Char('a'), Char('b'))

def test_transition_edges_split():
    tsx = NFA.transition_edges_split([
        (0, LEVEL1, 1),
        (1, LEVEL2, 2),
        (1, STANDBY1, 0),
        (2, STANDBY2, 0),
        (1, STANDBY1, 1),
    ])
    def tsx_func(src, ch):
        if src == 0 and ch == LEVEL1:
            return frozenset([1])
        if (src, ch) == (1, LEVEL2):
            return frozenset([2])
        if (src, ch) == (1, STANDBY1):
            return frozenset([0,1])
        if (src, ch) == (2, STANDBY2):
            return frozenset([0])
        return frozenset()
    for src in range(3):
        for k in [LEVEL1, LEVEL2, STANDBY1, STANDBY2]:
            assert tsx(src, k) == tsx_func(src, k)

def test_dfa_edges_1():
    n = create_hello_world()
    d = nfa_to_dfa(n)

    assert list(d.edges) == [
        (frozenset([0]), LEVEL1, frozenset([1])),
        (frozenset([0]), LEVEL2, frozenset()),
        (frozenset([0]), STANDBY1, frozenset()),
        (frozenset([0]), STANDBY2, frozenset()),
        #
        (frozenset([]), LEVEL1, frozenset()),
        (frozenset([]), LEVEL2, frozenset()),
        (frozenset([]), STANDBY1, frozenset()),
        (frozenset([]), STANDBY2, frozenset()),
        #
        (frozenset([1]), LEVEL1, frozenset()),
        (frozenset([1]), LEVEL2, set([2])),
        (frozenset([1]), STANDBY1, set([0])),
        (frozenset([1]), STANDBY2, frozenset()),
        #
        (frozenset([2]), LEVEL1, frozenset()),
        (frozenset([2]), LEVEL2, frozenset()),
        (frozenset([2]), STANDBY1, frozenset()),
        (frozenset([2]), STANDBY2, set([0])),
    ]

def test_dfa_iter_outgoing_1():
    n = create_hello_world()
    d = nfa_to_dfa(n)
    assert list(d.iter_outgoing()) == [
        (frozenset([0]),
        [
            (LEVEL1, set([1])),
            (LEVEL2, frozenset()),
            (STANDBY1, frozenset()),
            (STANDBY2, frozenset()),
        ]
        ),
        (frozenset([]),
        [
            (LEVEL1, frozenset()),
            (LEVEL2, frozenset()),
            (STANDBY1, frozenset()),
            (STANDBY2, frozenset()),
        ]
        ),
        (frozenset([1]),
        [
            (LEVEL1, frozenset()),
            (LEVEL2, set([2])),
            (STANDBY1, set([0])),
            (STANDBY2, frozenset()),
        ]
        ),
        (frozenset([2]),
        [
            (LEVEL1, frozenset()),
            (LEVEL2, frozenset()),
            (STANDBY1, frozenset()),
            (STANDBY2, set([0])),
        ]
        ),
    ]

def test_nfa_edges_1():
    n = create_hello_world()
    assert list(n.edges) == [
        (0, LEVEL1, set([1])),
        (0, LEVEL2, frozenset()),
        (0, STANDBY1, frozenset()),
        (0, STANDBY2, frozenset()),
        (0, None, frozenset()),
        #
        (1, LEVEL1, frozenset()),
        (1, LEVEL2, set([2])),
        (1, STANDBY1, set([0])),
        (1, STANDBY2, frozenset()),
        (1, None, frozenset()),
        #
        (2, LEVEL1, frozenset()),
        (2, LEVEL2, frozenset()),
        (2, STANDBY1, frozenset()),
        (2, STANDBY2, set([0])),
        (2, None, frozenset()),
    ]

def test_nfa_iter_outgoing_1():
    n = create_hello_world()
    assert list(n.iter_outgoing()) == [
        (0,
        [
            (LEVEL1, set([1])),
            (LEVEL2, frozenset()),
            (STANDBY1, frozenset()),
            (STANDBY2, frozenset()),
            (None, frozenset()),
        ]
        ),
        (1,
        [
            (LEVEL1, frozenset()),
            (LEVEL2, set([2])),
            (STANDBY1, set([0])),
            (STANDBY2, frozenset()),
            (None, frozenset()),
        ]
        ),
        (2,
        [
            (LEVEL1, frozenset()),
            (LEVEL2, frozenset()),
            (STANDBY1, frozenset()),
            (STANDBY2, set([0])),
            (None, frozenset()),
        ]
        ),
    ]

def test_nfa_state_diagram_1():
    n = create_hello_world()
    vs, es = n.as_graph()
    assert vs == set([0, 1, 2])
    assert es == {
        (0, 1): set([LEVEL1]),
        (1, 2): set([LEVEL2]),
        (1, 0): set([STANDBY1]),
        (2, 0): set([STANDBY2]),
    }

def test_minimize():
    """
    (0) --> LA_ON ---> (1)
    (1) --> LA_OFF --> (2)
    (2) --> LA_ON --> (3)
    (3) --> LA_OFF --> (0)
    """
    example = DFA(
        alphabet=[LA_ON, LA_OFF],
        transition_func=DFA.transition_table({
            (0, LA_ON): 1,
            (1, LA_OFF): 2,
            (2, LA_ON): 3,
            (3, LA_OFF): 0,
        }, sink=99),
        start_state=0,
        accepted_states=[0, 1, 2, 3],
    )
    # The above DFA has 5 states
    assert len(list(example.states)) == 5
    # Minimizing should yield only 3 states
    assert len(list(example.flatten(minimize=True).states)) == 3


def test_subtract():
    # Sigma*
    l = DFA(
        alphabet = [0, 1],
        transition_func = DFA.transition_table({
            ("s", 0): "s",
            ("s", 1): "s",
        }),
        start_state = "s",
        accepted_states = ["s"],
    )
    # Empty string
    r = DFA.make_nil([0, 1])
    result = l.subtract(r)
    expected = DFA(
        alphabet = [0, 1],
        transition_func = DFA.transition_table({
            ("s", 0): "q",
            ("s", 1): "q",
            ("q", 1): "q",
            ("q", 0): "q",
        }),
        start_state = "s",
        accepted_states = ["q"],
    )
    assert result.is_equivalent_to(expected)

def test_set_alphabet():
    # Accepts a*
    a = DFA(["a"],
        DFA.transition_table({(0, "a"): 0}),
        0, [0]
    )
    # Extend alphabet to accept a and b.
    b = a.set_alphabet(["a", "b"])
    assert b.accepts(["a", "a"])
    assert not b.accepts(["a", "b"])
    # Set alphabet to not contain "a"
    c = a.set_alphabet(["b"])
    assert c.is_equivalent_to(DFA.make_nil(["b"]))

def test_make_nil():
    c = NFA.make_nil(["a", "b"])
    assert [] in c
    assert "a" not in c
    assert "b" not in c
    assert "aa" not in c
    assert "ab" not in c
    assert "ba" not in c
    assert "bb" not in c

    c2 = DFA.make_nil(["a", "b"])
    assert [] in c2
    assert "a" not in c2
    assert "b" not in c2
    assert "aa" not in c2
    assert "ab" not in c2
    assert "ba" not in c2
    assert "bb" not in c2

    assert nfa_to_dfa(c).is_equivalent_to(c2)

def test_make_char():
    c = NFA.make_char(["a", "b"], "a")
    assert [] not in c
    assert "a" in c
    assert "b" not in c
    assert "aa" not in c
    assert "ab" not in c
    assert "ba" not in c
    assert "bb" not in c

    c2 = DFA.make_char(["a", "b"], "a")
    assert [] not in c2
    assert "a" in c2
    assert "b" not in c2
    assert "aa" not in c2
    assert "ab" not in c2
    assert "ba" not in c2
    assert "bb" not in c2

    assert nfa_to_dfa(c).is_equivalent_to(c2)


def test_make_any():
    c = NFA.make_any(["a", "b"])
    assert [] not in c
    assert "a" in c
    assert "b" in c
    assert "aa" not in c
    assert "ab" not in c
    assert "ba" not in c
    assert "bb" not in c

    c2 = DFA.make_any(["a", "b"])
    assert [] not in c2
    assert "a" in c2
    assert "b" in c2
    assert "aa" not in c2
    assert "ab" not in c2
    assert "ba" not in c2
    assert "bb" not in c2

    assert nfa_to_dfa(c).is_equivalent_to(c2)

def test_make_any_from():
    c = NFA.make_any_from(alphabet=["a", "b"], chars=["a"])
    assert [] not in c
    assert "a" in c
    assert "b" not in c
    assert "aa" not in c
    assert "ab" not in c
    assert "ba" not in c
    assert "bb" not in c

    c2 = DFA.make_any_from(alphabet=["a", "b"], chars=["a"])
    assert [] not in c2
    assert "a" in c2
    assert "b" not in c2
    assert "aa" not in c2
    assert "ab" not in c2
    assert "ba" not in c2
    assert "bb" not in c2

    assert nfa_to_dfa(c).is_equivalent_to(c2)

def test_make_void():
    c = NFA.make_void(alphabet=["a", "b"])
    assert [] not in c
    assert "a" not in c
    assert "b" not in c
    assert "aa" not in c
    assert "ab" not in c
    assert "ba" not in c
    assert "bb" not in c

    c2 = DFA.make_void(alphabet=["a", "b"])
    assert [] not in c2
    assert "a" not in c2
    assert "b" not in c2
    assert "aa" not in c2
    assert "ab" not in c2
    assert "ba" not in c2
    assert "bb" not in c2

    assert nfa_to_dfa(c).is_equivalent_to(c2)


def test_make_all():
    c = NFA.make_all(["a", "b"])
    assert [] in c
    assert "a" in c
    assert "b" in c
    assert "aa" in c
    assert "ab" in c
    assert "ba" in c
    assert "bb" in c

    c2 = DFA.make_all(["a", "b"])
    assert [] in c2
    assert "a" in c2
    assert "b" in c2
    assert "aa" in c2
    assert "ab" in c2
    assert "ba" in c2
    assert "bb" in c2

    assert nfa_to_dfa(c).is_equivalent_to(c2)


def test_dfa_as_dict():
    a = DFA(['a','b'], DFA.transition_table({
        (1,'a'): 1,
        (1,'b'): 2,
        (2,'a'): 3,
        (2,'b'): 2,
        (3,'a'): 2,
        (3,'b'): 2,
    }), 1, [2])
    d = a.as_dict(flatten=False)
    assert d["start_state"] == 1
    assert d["accepted_states"] == [2]
    assert list(sorted(d["edges"], key=lambda x:(x["src"], x["char"], x["dst"]))) == [
        dict(src=1, char='a', dst=1),
        dict(src=1, char='b', dst=2),
        dict(src=2, char='a', dst=3),
        dict(src=2, char='b', dst=2),
        dict(src=3, char='a', dst=2),
        dict(src=3, char='b', dst=2),
    ]
def test_dfa_get_derivation():
    a = DFA(['a','b'], DFA.transition_table({
        (1,'a'): 1,
        (1,'b'): 2,
        (2,'a'): 3,
        (2,'b'): 2,
        (3,'a'): 2,
        (3,'b'): 2,
    }), 1, [2])
    assert list(a.get_derivation(['a', 'b', 'a'])) == [1,2,3]
    assert list(a.get_derivation(['a', 'b'])) == [1, 2]
    assert list(a.get_derivation(['a', 'b', 'a', 'a'])) == [1,2,3,2]

def test_nfa_get_derivation():
    # Test a deterministic NFA:
    a = NFA(['a','b'], NFA.transition_edges_split([
        (1,'a', 1),
        (1,'b', 2),
        (2,'a', 3),
        (2,'b', 2),
        (3,'a', 2),
        (3,'b', 2),
    ]), 1, [2])
    assert list(a.get_derivations(['a', 'b', 'a'])) == [[1,2,3]]
    assert list(a.get_derivations(['a', 'b'])) == [[1, 2]]
    assert list(a.get_derivations(['a', 'b', 'a', 'a'])) == [[1,2,3,2]]

    # Test a self loop
    a = NFA(['a','b'], NFA.transition_edges_split([
        (1,None, 1),
    ]), 1, [1])
    assert list(a.get_derivations([])) == [[]]
    assert list(a.get_derivations("a")) == []

    # a* b+ a+
    a = NFA(['a','b'], NFA.transition_edges_split([
        (1,'a', 1),
        (1,'b', 2),
        (2,'b', 2),
        (2,'a', 3),
        (3, 'a', 3),
        (1, None, 4),
        (4, 'a', 5),
        (5, 'b', 6),
        (6, 'a', 7),
        (7, None, 3)
    ]), 1, [3, 7])
    assert list(sorted(a.get_derivations("aba"))) == [
        [1,2,3],
        [4,5,6,7],
    ]

def test_nfa_as_dict():
    a = NFA(['a','b'], NFA.transition_table({
        (1,'a'): [1],
        (1,'b'): [2],
        (2,'a'): [3],
        (2,'b'): [2],
        (3,'a'): [2],
        (3,'b'): [2],
    }), 1, [2])
    d = a.as_dict(flatten=False)
    assert d["start_state"] == 1
    assert d["accepted_states"] == [2]
    assert list(sorted(d["edges"], key=lambda x:(x["src"], x["char"], x["dst"]))) == [
        dict(src=1, char='a', dst=1),
        dict(src=1, char='b', dst=2),
        dict(src=2, char='a', dst=3),
        dict(src=2, char='b', dst=2),
        dict(src=3, char='a', dst=2),
        dict(src=3, char='b', dst=2),
    ]

def test_dfa_from_dict():
    dfa1 = dict(
        start_state=1,
        accepted_states=[2],
        edges=[
            dict(src=1, char='a', dst=1),
            dict(src=1, char='b', dst=2),
            dict(src=2, char='a', dst=3),
            dict(src=2, char='b', dst=2),
            dict(src=3, char='a', dst=2),
            dict(src=3, char='b', dst=2),
        ]
    )
    d = DFA.from_dict(dfa1)
    assert d == DFA(
        set(['a','b']),
        DFA.transition_table({
            (1,'a'): 1,
            (1,'b'): 2,
            (2,'a'): 3,
            (2,'b'): 2,
            (3,'a'): 2,
            (3,'b'): 2,
        }),
        1,
        [2]
    )

def test_nfa1_from_dict():
    nfa1 = dict(
        start_state=1,
        accepted_states=[2],
        edges=[
            dict(src=1, char='a', dst=1),
            dict(src=1, char='b', dst=2),
            dict(src=2, char='a', dst=3),
            dict(src=2, char='b', dst=2),
            dict(src=3, char='a', dst=2),
            dict(src=3, char='b', dst=2),
        ]
    )
    d = NFA.from_dict(nfa1)
    assert d == NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([1]),
            (1,'b'): set([2]),
            (2,'a'): set([3]),
            (2,'b'): [2],
            (3,'a'): [2],
            (3,'b'): [2],
        }),
        1,
        [2]
    )

def test_nfa_flatten():
    n1 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([1]),
            (1,'b'): set([2]),
            (2,'a'): set([3]),
            (2,'b'): [2],
            (3,'a'): [2],
            (3,'b'): [2],
        }),
        1,
        [2]
    )
    n1_aux = nfa_to_dfa(n1)
    n2 = dfa_to_nfa(n1_aux).flatten()
    assert nfa_to_dfa(n1).is_equivalent_to(nfa_to_dfa(n2))

def test_nfa_map_alphabet():
    n1 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([1]),
            (1,'b'): set([2]),
            (2,'a'): set([3]),
            (2,'b'): [2],
            (3,'a'): [2],
            (3,'b'): [2],
        }),
        1,
        [2]
    )
    given = n1.map_alphabet({"a": "b", "b": "a", None: None})
    expected = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'b'): set([1]),
            (1,'a'): set([2]),
            (2,'b'): set([3]),
            (2,'a'): [2],
            (3,'b'): [2],
            (3,'a'): [2],
        }),
        1,
        [2]
    )

    assert expected == given

    given = n1.map_alphabet({"b": "d", "a": "d", None: None})
    expected = NFA(
        set(['d']),
        NFA.transition_table({
            (1,'d'): set([1,2]),
            (2,'d'): set([3, 2]),
            (3,'d'): set([2]),
        }),
        1,
        [2]
    )
    assert expected == given

    given = n1.map_alphabet({"b": None, "a": "d", None: None})
    expected = NFA(
        set(['d']),
        NFA.transition_table({
            (1,'d'): set([1]),
            (1,None): set([2]),
            (2,'d'): set([3]),
            (2,None): [2],
            (3,'d'): [2],
            (3,None): [2],
        }),
        1,
        [2]
    )
    assert expected == given


def test_nfa_remove_epsilon_transitions():
    n1 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([1]),
            (1,None): set([2]),
            (2, 'a'): set([4]),
            (2,None): set([3]),
            (2,'b'): [2],
            (3,'a'): [2],
            (3,'b'): [2],
        }),
        1,
        [2]
    )
    n2 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([1, 2, 4]),
            (1,'b'): set([2]),
            (2, 'a'): set([2, 4]),
            (2,'b'): set([2]),
            (3,'a'): set([2]),
            (3,'b'): set([2]),
        }),
        1,
        [1, 2]
    )
    assert n1.remove_epsilon_transitions() == n2


def test_nfa_remove_epsilon_transitions_2():
    n1 = NFA(
        {'a.on', "a.off",'t.s', "t.t", "t.c", "b.on", "b.off"},
        NFA.transition_edges_split([
            (0, None, 4),
            (4, "a.on", 6),
            (6, "t.s", 7),
            (7, None, 8),
            (8, None, 9),
            (8, None, 10),
            (10, "t.t", 33),
            (33, "a.off", 34),
            (34, None, 35),
            (35, None, 4),
            (9, "t.c", 36),
            (36, "b.on", 38),
            (38, "t.s", 39),
            (39, None, 40),
            (40, None, 41),
            (41, "t.t", 43),
            (43, "b.off", 44),
            (43, "a.off", 45),
            (44, "a.off", 48),
            (45, "b.off", 46),
            (48, None, 47),
            (46, None, 47),
            (47, None, 4),
        ]),
        0,
        [35, 47]
    ).remove_epsilon_transitions()
    n2 = NFA(
        {'a.on', "a.off",'t.s', "t.t", "t.c", "b.on", "b.off"},
        NFA.transition_edges_split([
            (0, "a.on", 6),
            (6, "t.s", 7),
            (7, "t.c", 36),
            (7, "t.t", 33),
            (33, "a.off", 34),
            (34, "a.on", 6),
            (9, "t.c", 36),
            (36, "b.on", 38),
            (38, "t.s", 39),
            (39, "t.t", 43),
            (43, "b.off", 44),
            (43, "a.off", 45),
            (44, "a.off", 48),
            (45, "b.off", 46),
            (48, "a.on", 6),
            (46, "a.on", 6),
        ]),
        0,
        [34, 48, 46]
    )
    assert list(sorted(n1.states)) == list(sorted(n2.states))
    assert list(sorted(filter(n1.accepted_states, n1.states))) == list(sorted(filter(n2.accepted_states, n2.states)))
    for st in n1.states:
        assert dict(n1.get_outgoing(st)) == dict(n2.get_outgoing(st)), f"state {st}"
    assert n1 == n2

def test_nfa_remove_sink_states():
    n1 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([2]),
            (2, 'a'): set([3]),
            (3,'a'): [3],
            (3,'b'): [3],
        }),
        1,
        [2]
    )
    n2 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([2]),
            (2, 'a'): set([]),
        }),
        1,
        [2]
    )
    assert n1.remove_sink_states() == n2

def test_nfa_remove_all_sink_states():
    n1 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([2]),
            (2, 'a'): set([3]),
            (3,'a'): [3],
            (3,'b'): [3],
        }),
        1,
        [2]
    )
    n2 = NFA(
        set(['a','b']),
        NFA.transition_table({
            (1,'a'): set([2]),
            (2, 'a'): set([]),
        }),
        1,
        [2]
    )
    assert n1.remove_sink_states() == n2
