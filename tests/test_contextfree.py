from karakuri.contextfree import PDA, Transition, Computation, Node, Link, Action

A = 'A'
B = 'B'
C = 'C'
Empty = "$"

def test_pda1():
    a = PDA(
        transitions={
            # Bootstrap
            ("q", None, None): Transition(Empty, "q_{a,i}"),
            # read a^i
            ("q_{a,i}", A, None): Transition(A, "q_{a,i}"),
            ("q_{a,i}", None, None): Transition(None, "q_{b,i}"),
            # read b^i
            ("q_{b,i}", B, A): Transition(None, "q_{b,i}"),
            ("q_{b,i}", None, None): Transition(None, "q_{b,k}"),
            # Read b^k
            ("q_{b,k}", B, None): Transition(B, "q_{b,k}"),
            ("q_{b,k}", None, None): Transition(None, "q_{a,k}"),
            # read a^k
            ("q_{a,k}", A, B): Transition(None, "q_{a,k}"),
            ("q_{a,k}", None, Empty): Transition(None, "q_{y}"),
        },
        start_state="q",
        accepted_states=["q_{y}"]
    )
    assert a.accepts([A, A, B, B, B, A])
    assert not a.accepts([A, A, B, B, B, A, A])
    assert not a.accepts([A, A, B, B, A, A])
    assert a.accepts([A, B, B, A])
    assert a.accepts([])
    assert a.accepts([A, B])
    assert a.accepts([B, A])

def test_pda2():
    ##############################################################################
    # Exercise 3
    a = PDA(
        transitions={
            #Bootstrap
            ("q", None, None): Transition(Empty, "q_{a}"),
            # read a^i
            ("q_{a}", A, None): Transition(A, "q_{a}"),
            ("q_{a}", None, None): Transition(None, "q_{b,1}"),
            # read b^i
            ("q_{b,1}", B, A): Transition(None, "q_{b,2}"),
            ("q_{b,2}", B, None): Transition(None, "q_{b,1}"),
            ("q_{b,1}", None, Empty): Transition(None, "q_y"),
        },
        start_state="q",
        accepted_states=["q_y"]
    )
    assert not a.accepts([A,A,A,B,A])

def test_pda3():
    ##############################################################################
    # Exercise 3
    a = PDA(transitions={
            ("q_1", A, None): Transition(A, "q_1"),
            ("q_1", None, None):
            {Transition(None, "q_2"),
            Transition(None, "q_4")},
            ("q_2", B, A): Transition(None, "q_2"),
            ("q_2", C, Empty): Transition(Empty, "q_3"),
            ("q_3", C, None): Transition(None, "q_3"),
            ("q_4", B, None): Transition(None, "q_4"),
            ("q_4", C, A): Transition(None, "q_5"),
            ("q_5", C, A): Transition(None, "q_5"),
            ("q_5", None, Empty): Transition(Empty, "q_6"),
        },
        start_state="q_1",
        accepted_states=["q_1", "q_3", "q_6"]
    )


def test_pda4():
    ##############################################################################
    # Exercise 4
    a = PDA(transitions={
            ("q", None, None): Transition(Empty, "q_1"),
            ("q_1", A, None): Transition(A, "q_1"),
            ("q_1", None, None): Transition(None, "q_2"),
            ("q_2", B, A): Transition(None, "q_2"),
            ("q_2", None, Empty): Transition(None, "q_3"),
        },
        start_state="q",
        accepted_states=["q_3"]
    )
    assert a.accepts([A, A, B, B])

def test_pda5():
    ## Finally we can test this
    A = 'a'
    B = 'b'
    C = 'c'
    Empty = "$"
    a = PDA(
        transitions={
            # Bootstrap
            ("q1", None, None): Transition(Empty, "q2"),
            ("q2", A, None): Transition(A, "q2"),
            ("q2", B, A): Transition(None, "q3"),
            ("q3", B, A): Transition(None, "q3"),
            ("q3", None, Empty): Transition(None, "q4"),
        },
        start_state="q1",
        accepted_states=["q4"]
    )
    assert not a.accepts("a")
    accepts = [
        "ab",
        "aabb",
        "aaabbb",
        "aaaabbbb",
    ]
    rejects = [
        "",
        "a",
        "b",
        "aa",
        "bb",
        "ba",
        "aaa",
        "aab",
        "aba",
        "abb",
        "baa",
        "bab",
        "bbb",
    ]
    for x in accepts:
        assert a.accepts(x)
    for x in rejects:
        assert not a.accepts(x)



def test_pda6():
    ## Finally we can test this
    A = 'a'
    B = 'b'
    C = 'c'
    Empty = "$"
    a = PDA(
        transitions={
            # Bootstrap
            ("q1", None, None): {
                Transition(Empty, "q2"),
                # Self loop, will generate many reductions
                Transition(Empty, "q1")
            },
            ("q2", A, None): Transition(A, "q2"),
            ("q2", B, A): Transition(None, "q3"),
            ("q3", B, A): Transition(None, "q3"),
            ("q3", None, Empty): Transition(None, "q4"),
        },
        start_state="q1",
        accepted_states=["q4"]
    )
    assert not a.accepts("a")
    accepts = [
        "ab",
        "aabb",
        "aaabbb",
        "aaaabbbb",
    ]
    rejects = [
        "",
        "a",
        "b",
        "aa",
        "bb",
        "ba",
        "aaa",
        "aab",
        "aba",
        "abb",
        "baa",
        "bab",
        "bbb",
    ]
    for x in accepts:
        assert a.accepts(x)
    for x in rejects:
        assert not a.accepts(x)


def test_pda7():
    ## Finally we can test this
    A = 'a'
    B = 'b'
    C = 'c'
    Empty = "$"
    a = PDA(
        transitions={
            # Bootstrap
            ("q1", None, None): {
                Transition(Empty, "q2"),
                # Self loop, will generate many reductions
                Transition("x", "q1")
            },
            ("q2", A, None): {Transition(A, "q2"),
                Transition("x", "q2")
            },
            ("q2", B, A): Transition(None, "q3"),
            ("q3", B, A): Transition(None, "q3"),
            ("q3", None, Empty): Transition(None, "q4"),
        },
        start_state="q1",
        accepted_states=["q4"]
    )
    accepts = [
        "ab",
        "aabb",
        "aaabbb",
        "aaaabbbb",
    ]
    rejects = [
        "",
        "a",
        "b",
        "aa",
        "bb",
        "ba",
        "aaa",
        "aab",
        "aba",
        "abb",
        "baa",
        "bab",
        "bbb",
    ]
    for x in accepts:
        assert a.accepts(x)
    for x in rejects:
        assert not a.accepts(x)


def parse_pda(data):
    edges = dict()
    for e in data["edges"]:
        key = e["src"], e.get("char"), e.get("pop")
        curr = edges.get(key)
        if curr is None:
            edges[key] = curr = set()
        curr.add(Transition(push_arg=e.get("push"), dst=e["dst"]))

    return PDA(
        transitions=edges,
        start_state=data["start_state"],
        accepted_states=data["accepted_states"],
    )

def test_pda8():
    import yaml
    d = yaml.safe_load("""
  start_state: "q0"
  accepted_states: [qf]
  edges:
    - {src: q0, dst: qa, push: $}

    - {src: qa, dst: qa, char: a, push: a}

    - {src: qa, dst: qa1, push: a}
    - {src: qa1, dst: qb, pop: a}

    - {src: qb, dst: qb, push: a, char: b}
    
    - {src: qb, dst: qb1, push: a}
    - {src: qb1, dst: qc, pop: a}

    - {src: qc, dst: qc, char: c, pop: a}
    - {src: qc, dst: qf, pop: $}
    """)
    p = parse_pda(d)
    assert p.accepts("")


def test_pda9():
    ## Finally we can test this
    A = 'a'
    B = 'b'
    C = 'c'
    Empty = "$"
    a = PDA(
        transitions={
            # Bootstrap
            ("q1", None, None): Transition(Empty, "q2"),
            ("q2", None, None): {
              Transition(A, "q2"),
              Transition(None, "q3")
            },
            ("q3", A, A): Transition(None, "q3"),
            ("q3", None, Empty): Transition(None, "q4"),
        },
        start_state="q1",
        accepted_states=["q4"]
    )
    accepts = [
        "",
        "a",
        "aa",
    ]
    rejects = [
        "b",
        "bb",
        "ba",
        "aab",
        "aba",
        "abb",
        "baa",
        "bab",
        "bbb",
    ]
    for x in accepts:
        assert a.accepts(x)
    for x in rejects:
        assert not a.accepts(x)


    c = a.get_computation(["a", "a"])
    assert c.final == {Node(state="q4", index=2)}
    assert c.start == Node(state="q1", index=0)
    assert len(c.steps) == 5
    q2_0 = Node(index=0, state='q2')
    assert c.steps[Node(index=0, state='q1')] == [
        Link(action=Action(read_char=None, pop_char=None, push_char='$'), dst=q2_0)
    ]
    epsilon = Action(read_char=None, pop_char=None, push_char=None)
    q3_0 = Node(index=0, state='q3')
    q3_1 = Node(index=1, state='q3')
    q3_2 = Node(index=2, state='q3')
    q4_0 = Node(index=0, state='q4')
    q4_1 = Node(index=1, state='q4')
    q4_2 = Node(index=2, state='q4')
  
    assert set(c.steps[q2_0]) == {
        Link(action=Action(read_char=None, pop_char=None, push_char='a'), dst=q2_0),
        Link(action=Action(read_char=None, pop_char=None, push_char=None), dst=q3_0),
    }
    assert set(c.steps[q3_0]) == {
        Link(action=Action(read_char='a', pop_char='a', push_char=None), dst=q3_1),
        Link(action=Action(read_char=None, pop_char='$', push_char=None), dst=q4_0),
    }
    assert set(c.steps[q3_1]) == {
        Link(action=Action(read_char='a', pop_char='a', push_char=None), dst=q3_2),
        Link(action=Action(read_char=None, pop_char='$', push_char=None), dst=q4_1),
    }
    assert set(c.steps[q3_2]) == {
        Link(action=Action(read_char=None, pop_char='$', push_char=None), dst=q4_2),
    }
    c = c.reduce()
    assert c.reaches_final()